'use strict';

const textrank =  require('textrank-node');
const URL = require('url');
import * as RequestsVideo from './../tools/actions/RequestsVideo';
import _ from 'lodash';

let filtersPromise = function (data, callback) {

    RequestsVideo.getFilters((response) => {
        let contains = false;
        response.forEach((string)=>{
            if(_.includes(data, string.filter)){
                contains=true;
                return null;
            }
        });

        if(!contains){
            callback(false);
        }else{
            callback(true);
        }

    });

};

let segregate = function(data){
    let prefixes = ['Dr', 'Sr', 'Sra', 'Mag', 'D', 'Ex', 'V', 'S', 'Rev', 'n', 'www', 'http://www', 'Sra', 'Mr', 'Ms'];
    let sufixes = ['pt', 'com', 'es', 'gov', 'º'];
    let trunc = false;
    let resString = "";
    let res = [];

    for (let i = 0; i < data.length; i++){
        resString += (data[i]);

        if (data[i] === "." && data[i+1] === " "){
            trunc = true;

            prefixes.forEach(el=> {
                el = el.toLocaleLowerCase();
                if (data.substr(i - el.length,el.length).toLowerCase() === el){
                    if(data[i - el.length - 1] === " "){
                        trunc = false;
                    }
                }
            });

            sufixes.forEach(el=> {
                el = el.toLocaleLowerCase();
                if (data.substr(i + 1, el.length).toLowerCase() === el){
                    trunc = false;
                }
            });

            if (trunc){
                res.push(resString);
                resString = "";
            }
        }
    }

    //push what is left of strings after last dot
    if(resString != ""){
        res.push(resString);
        resString = "";
    }

    return res;
};

let splitter = function (data, l) {
    let strs = [];
    while(data.length > l){
        let pos = data.substring(0, l).lastIndexOf(' ');
        pos = pos <= 0 ? l : pos;
        strs.push(data.substring(0, pos));
        let i = data.indexOf(' ', pos)+1;
        if(i < pos || i > pos+l)
            i = pos;
        data = data.substring(i).replace(/\n/g, '');
        data = data.replace(/\s\s+/g, ' ');
    }
    strs.push(data);
    return strs;
};

let splitterWithSpaces = function (data, l) {
    let strs = [];
    while(data.length > l){
        let pos = data.substring(0, l).lastIndexOf(' ');
        pos = pos <= 0 ? l : pos;
        strs.push(data.substring(0, pos));
        let i = data.indexOf(' ', pos);
        if(i < pos || i > pos+l)
            i = pos;
        data = data.substring(i).replace(/\n/g, '');
        data = data.replace(/\s\s+/g, ' ');
    }
    strs.push(data);

    return strs;
};

let substring = function (data, start, len) {

    //trim the string to the maximum length
    let trimmedString = data.substring(start, len);
    let result = trimmedString;
    //re-trim if we are in the middle of a word

    if(data[len+1]!= undefined){
        let char = data[len+1];
        if (char != " ") {
            result = trimmedString.substring(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")));
        }
    }

    return result;
};

let splitSections = function (data, words, lines){
    let lastTextIndex = 0;
    let splitText  = splitter(data, words).length;

    let text = [];

    for (let i=0;i<splitText;i++){
        while (data.length > words && data.substr(words-1,1) != " ") words++;
        text.push(data.substring(lastTextIndex, words+lastTextIndex));
        lastTextIndex += words;
    }

    let newArray = [];
    let concatText = '';

    for (let i = 0; i < text.length; i++){
        concatText += text[i];
        if(i % lines === 0){
            newArray.push(concatText);
            concatText = '';
        }
    }

    return newArray;
};

let splitOnPosition = function (_data, _textSize, start, end) {
    let textSize = _textSize;
    let data = _data;
    while (data.length > textSize && data.substr(textSize-1,1) != " ") textSize++;
    return data.substring(start, end);
};

let summarize = function (data, sentences){
    let summarizer = new textrank();
    let result = summarizer.summarize(data, sentences);
    return result;
};

let handleURL = function (host,path){
    if (!path)
        path = "";

    let result = URL.resolve(host,path);
    return result;
};

let parseURL = function (host){
    let result = URL.parse(host);
    return result;
};

let ffmpegEscape = function (data){
    let result =  data.replace(":", "\\:");
    result = _.escape(result);
    return result;
};

let generalEscape = function (data){
    let result =  data.replace(/ *\([^)]*\) */g, ' ').replace(/\n/g, '').replace(/\r/g, '');
    return result;
};


module.exports = {
    filtersPromise,
    segregate,
    splitter,
    splitterWithSpaces,
    substring,
    splitSections,
    splitOnPosition,
    summarize,
    handleURL,
    parseURL,
    ffmpegEscape,
    generalEscape
};