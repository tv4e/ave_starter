'use strict';
//Request is designed to be the simplest way possible to make http calls.
// It supports HTTPS and follows redirects by default.
import axios from 'axios';
import Store from '../store/Store';
import request from 'request';
import query_string from 'query-string';
import {requestsLog} from './../Logger';

export function login(callback){
    let payload = {
        grant_type: 'password',
        client_id: 2,
        client_secret: '2swKu8IxdF2BrTKXJUamnotBuC1dda8c1pcoZOVU',
        username: 'the.misterie@gmail.com',
        password: 'madiba'
    };

    axios({
        method:'post',
        url:`${Store.api}/oauth/token`,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept' : 'application/x-www-form-urlencoded'
        },
        data: query_string.stringify(payload)
    })
    .then((response) => {
        if (response.status != 200) {
            requestsLog('Error | Login');
            console.log('error');
            throw Error(response.statusText);
        }
        response = response.data;
        requestsLog('Success | Login');

        Store.token = response['access_token'];
        callback(response);
    })
    .catch((error) => {
        requestsLog('Error | Login');
        console.log('error',error);
    });

}

export function allAsgie(callback){
    axios({
        method:'get',
        url:`${Store.api}/api/asgies`,
        headers:{
            'Authorization' : 'Bearer '  + Store.token
        }
    })
        .then((response) => {
            if (response.status != 200) {
                requestsLog('Error | allAsgie');
                console.log('allAsgie: error');
                throw Error(response.statusText);
            }
            requestsLog('Success | allAsgie');
            response = response.data;
            callback(response);
        })
        .catch(() => {
            requestsLog('Error | allAsgie');
            console.log('allAsgie: error');
        });
}

export function getAsgie(id,callback){
    axios({
        method:'get',
        url:`${Store.api}/api/asgie/${id}`,
        headers:{
            'Authorization' : 'Bearer '  + Store.token
        }
    })
        .then((response) => {
            if (response.status != 200) {
                requestsLog('Error | getAsgie');
                console.log('getAsgie: error');
                throw Error(response.statusText);
            }
            requestsLog('Success | getAsgie');
            response = response.data;
            callback(response);
        })
        .catch((error) => {
            requestsLog('Error | getAsgie');
            console.log('getAsgie: error');
            console.log(error);
        });
}

export function checkVideo(title, source, sub, callback){
    axios({
        method:'GET',
        url:`${Store.api}/api/videos/check`,
        params:{
            title:title,
            source:source,
            sub:sub
        },
        headers:{
            'Authorization' : 'Bearer '  + Store.token
        }
    })
    .then((response) => {

        if (response.status != 200) {
            requestsLog('Error | checkVideo');
            console.log('checkVideo: error');
            throw Error(response.statusText);
        }
        requestsLog('Success | checkVideo');
        response = response.data;
        callback(response);
    })
    .catch((error) => {
        requestsLog('Error | checkVideo');
        console.log('checkVideo: ',error);
        console.log('checkVideo: error');
    });
}

export function getSource(callback){
    axios({
        method:'get',
        url:`${Store.api}/api/sources`,
        headers:{
            'Authorization' : 'Bearer '  + Store.token
        }
    })
    .then((response) => {
        if (response.status != 200) {
            requestsLog('Error | getSource');
            console.log('getSource: error');
            throw Error(response.statusText);
        }
        requestsLog('Success | getSource');
        response = response.data;
        callback(response);
    })
    .catch((error) => {
        requestsLog('Error | getSource');
        console.log('getSource: error');
        console.log(error);
    });
}


export function getDistricts(callback){
    axios({
        method:'get',
        url:`${Store.api}/api/districts`,
        headers:{
            'Authorization' : 'Bearer '  + Store.token
        }
    })
        .then((response) => {
            if (response.status != 200) {
                requestsLog('Error | getDistricts');
                console.log('getDistricts: error');
                throw Error(response.statusText);
            }
            requestsLog('Success | getDistricts');
            response = response.data;
            callback(response);
        })
        .catch((error) => {
            requestsLog('Error | getDistricts');
            console.log('getDistricts: error');
            console.log(error);
        });
}


export function getFilters(callback){

    if(Store.filters != null){
        callback(Store.filters);
        return null;
    }

    axios({
        method:'get',
        url:`${Store.api}/api/filters`,
        headers:{
            'Authorization' : 'Bearer '  + Store.token
        }
    })
        .then((response) => {
            if (response.status != 200) {
                requestsLog('Error | getFilters');
                console.log('getFilters: error');
                throw Error(response.statusText);
            }
            requestsLog('Success | getFilters');
            response = response.data;
            Store.filters = response;
            callback(response);

        })
        .catch((error) => {
            requestsLog('Error | getFilters');
            console.log('getFilters: error');
            console.log(error);
        });
}


export function putVideo(data, callback){
    if(Array.isArray(data.desc)){
        data.desc = JSON.stringify(data.desc);
    }

    let videoObject = {
        "desc": data.desc,
        "filename": data.filename,
        "title": data.title,
        "duration": data.duration
    };

    if(data.isSub == true){
        videoObject["source_sub_id"] = data.source_id;
    }else{
        videoObject["source_id"] = data.source_id;
    }

    axios({
            url:`${Store.api}/api/videos`,
            method: 'POST',
            data : videoObject,
            headers:{
                'Authorization' : 'Bearer '  + Store.token
            }
    })
    .then((response) => {
        if (response.status != 200){
            requestsLog('Error | putVideo');
            console.log('Error Updating Sources', err);
            return null;
        }
        requestsLog('Success | putVideo');
        callback();
    })
    .catch((error) => {
        requestsLog('Error | putVideo');
        console.log('error',error);
    });
}