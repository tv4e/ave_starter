'use strict';
import _ from 'lodash';

class CartaSocial {

    constructor() {
        this.callback = null;
        this.resposta=[];
    }

    handle(data, city, resposta) {
        let content ={
            title: `${city} | ${resposta}`,
            description: []
        };

        _.forEach(data, (resposta)=>{
            content.description.push(`${resposta.title} - ${resposta.description}`);
            content.description.push(``);
        });

        return (content);
    }
}

export default (new CartaSocial);
