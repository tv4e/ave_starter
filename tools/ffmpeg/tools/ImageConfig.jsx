"use strict";

import {vidRender} from './../../../configs/Config.jsx';
import Polly from './../../actions/AwsPolly';
import {createCanvas, loadImage} from 'canvas';
var CanvasTextWrapper = require('canvas-text-wrapper').CanvasTextWrapper;
import _ from 'lodash';

export default function ImageConfig(video, callback){

    let slides = [];
    let slidesNrs = [];
    let tts = [];

    video.slides.forEach((slide)=>{
        if (slide.narrate!=undefined){
            tts.push(slide.narrate);
        }
    });

    new Polly(tts,
        `${video.tempPath}/${video.filename}`, (audioTime)=>{
            process(audioTime);
        }
    ).synth();

    function process(audioTime){

        let bestFit = [];
        let slidesNumber = [];
        let slidesCompleted = 0;

        const canvas = createCanvas(vidRender.width, vidRender.height);
        const ctx = canvas.getContext('2d');
        canvas.width = video.width;
        canvas.height = video.height;

        video.slides.forEach((slide, bestFitIndex)=>{
            bestFit.push([]);
            slide.txtBoxes.forEach((txtBox , txtK)=>{
                let fitText = '';

                if (typeof txtBox.text != 'string'){
                    txtBox.text.forEach((textBlock)=>{
                        fitText += _.isEmpty(textBlock)?"#iamspace#--":textBlock;
                    });
                }else{
                    fitText = txtBox.text;
                }

                let fit = CanvasTextWrapper(
                    canvas,
                    fitText,
                    _.merge({
                        font: `${txtBox.fontSize}`,
                        allowNewLine: false
                    }, txtBox.options)
                );

                bestFit[bestFitIndex].push(fit);

                //TRUNCATE BY REMOVING LINES MORE THAN THE ONES ASKED
                //USE TRUNCATE WITH OMISSION LENGTH AND TEXT LENGTH TO SET NUMBER OF CHARS FOR THE LAST INDEX
                if(txtBox.truncate){
                    if(bestFit[bestFitIndex][bestFit[bestFitIndex].length-1].length > txtBox.maxLines ) {
                        bestFit[bestFitIndex][bestFit[bestFitIndex].length-1] = _.dropRight(bestFit[bestFitIndex][bestFit[bestFitIndex].length-1], bestFit[bestFitIndex][bestFit[bestFitIndex].length-1].length - txtBox.maxLines);
                        bestFit[bestFitIndex][bestFit[bestFitIndex].length-1][bestFit[bestFitIndex][bestFit[bestFitIndex].length-1].length-1] = _.truncate(
                            bestFit[bestFitIndex][bestFit[bestFitIndex].length-1][bestFit[bestFitIndex][bestFit[bestFitIndex].length-1].length-1], {
                            'omission': txtBox.truncate
                        });
                    }
                }

                //ADJUST POSITION Y IF ADAPTABLE ACCORDING TO DISTANCE BETWEEN LAST INDEX
                // NEW POSITIONY = POSITIONY - PREVIOUS_POSITIONY +(PREVIOUS_POSITIONY*NR_LINES)
                if(txtBox.adaptable == true) {
                    txtBox.options.paddingY = txtBox.options.paddingY - slide.txtBoxes[txtK-1].options.paddingY + (slide.txtBoxes[txtK-1].options.paddingY * (bestFit[bestFitIndex][bestFit[bestFitIndex].length-2].length));
                }

                if(txtBox.followContent){
                    slidesNrs.push(Math.ceil(fit.length/txtBox.maxLines));
                }
            });
        });

        let totalSlidesNumber = slidesNrs.reduce((a, b) => a + b, 0);
        let imageSlides = [];

        video.images.forEach((image)=> {
            imageSlides.push(Math.ceil(image.loopTimeRatio * totalSlidesNumber));
        });

        let lstEmptyIndex = 0;

        slidesNrs.forEach((nrSlides, indexSlides)=>{
            for(let i = 0; i < nrSlides; i++) {
                slides.push({
                    path: '',
                    image: '',
                    options: '',
                    body: [],
                    loopTime: 0,
                    index: slidesCompleted
                });

                video.images.forEach((image, imageIndex)=> {
                    if(lstEmptyIndex == imageIndex){
                        imageSlides[imageIndex] -=1;
                        slides[slidesCompleted].image = image.imagePath;
                        slides[slidesCompleted].path = `${video.tempPath}/tmpImage${slidesCompleted}`;
                        slides[slidesCompleted].options = image.options;

                        if(imageSlides[imageIndex]==0){
                            lstEmptyIndex= imageIndex ;
                        }
                    }
                });

                let slide = video.slides[indexSlides];

                //SET  TIME FOR SLIDES
                slide.txtBoxes.forEach((txtBox,indexTxtBox)=>{

                    if(txtBox.followContent){

                        slides[slidesCompleted].body.push({
                            text: bestFit[indexSlides][indexTxtBox].slice((i)*txtBox.maxLines, (txtBox.maxLines+((i)* txtBox.maxLines))).join(' '),
                            options: txtBox.options,
                            fontSize: txtBox.fontSize
                        });

                    } else {

                        slides[slidesCompleted].body.push({
                            text : bestFit[indexSlides][indexTxtBox].slice(0, txtBox.maxLines).join(' '),
                            options: txtBox.options,
                            fontSize: txtBox.fontSize
                        });

                    }

                    if(slide.loop != undefined){
                        slides[slidesCompleted].loopTime = slide.loop;
                    }else{
                        slides[slidesCompleted].loopTime = ((audioTime[indexSlides] *  slides[slidesCompleted].body[slides[slidesCompleted].body.length-1].text.length)/_.join(bestFit[indexSlides][indexTxtBox], ' ').length);
                    }

                });
                slidesCompleted++;
            }
        });


        slides.forEach((slide, k) => {
            loadImage(slide.image).then((image) => {
                const canvas = createCanvas(vidRender.width, vidRender.height);
                const ctx = canvas.getContext('2d');
                canvas.width = video.width;
                canvas.height = video.height;

                ctx.drawImage(image, 0, 0, vidRender.width, vidRender.height);

                ctx.beginPath();
                ctx.rect(0, 0, vidRender.width, vidRender.height);

                let overlayColor = hexToRgb(slide.options.overlay);
                ctx.fillStyle = `rgba(${overlayColor.r},${overlayColor.g},${overlayColor.b},0.8)`;
                ctx.fill();

                ctx.fillStyle = 'rgba(255,255,255,1)';

                slide.body.forEach((bodyTxt) => {
                    bodyTxt.text = bodyTxt.text.replace(/#iamspace#--/g,'\n\n');

                    CanvasTextWrapper(
                        canvas,
                        bodyTxt.text,
                        _.merge({
                            font: `${bodyTxt.fontSize}`,
                            allowNewLine: false
                        }, bodyTxt.options)
                    );
                });

                var fs = require('fs')
                    , out = fs.createWriteStream(slide.path)
                    , stream = canvas.pngStream();

                stream.on('data', function(chunk){
                    out.write(chunk);
                });

                stream.on('end', function(){
                    console.log('saved png');
                    if(k == slides.length-1){
                        callback(slides);
                    }
                });
            });
        });

    }

    function hexToRgb(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

}