'use strict';

import HandleText from './../HandleText';
import {dirs,vidRender} from './../../configs/Config.jsx';
import filters from './tools/filters';
import ImageConfig from './tools/ImageConfig';
import MergeVideos from './tools/MergeVideos';
import ImageToVideo from './tools/ImageToVideo';
import _ from 'lodash';

export default class DynamicVideo {
    constructor(data, callback) {
        //are private for the instances created
        this.audio = null;
        this.callback = callback;

        try {
            this.create(data);
        } catch(error) {
            console.log('DynamicVideo: error');
            console.log(error);
        }
    }

    create(data) {

        // console.log('\n\nOriginal*************************************************');
        // console.log(data.desc);
        // console.log('Original*************************************************');

        let filename = data.filename;
        let tempPath = data.tempPath;
        let media = data.media;
        let foregroundColor = data.foregroundColor;
        let desc = data.desc;
        let title = data.title;
        let source = data.source;
        let _imagesConverted = 0;
        let images = null;

        // console.log('\n\nParagraph-------------------------------------------------');
        // console.log(desc);
        // console.log('Paragraph-------------------------------------------------');

        let video = null;

        if (_.includes(source.url, 'www.cartasocial')){
            video = videoCartaSocial();
        } else {
            video = videoNormal();
        }

        media['image'].forEach((file,index) => {
            if(file.selected){
                //time that each image is onScreen according to its duration in percentage
                let ratio = file.duration/100;

                video.images.push({
                    loopTimeRatio : ratio,
                    imagePath: dirs.root + HandleText.parseURL(file.url).pathname,
                    options:{
                        overlay: foregroundColor
                    }
                });
            }
        });

       ImageConfig(video,(_images)=>{
           images = _images;
            convert.call(this);
        });

        function convert() {
            for (let i = 0; i < images.length; i++) {
                let callback = checkImagesConverted.bind(this);
                ImageToVideo.push(images[i], tempPath, callback);
            }
        }

        function checkImagesConverted() {
            _imagesConverted++;

            if (_imagesConverted == images.length) {
                merge.call(this);
            }
        }

        function merge() {
            let music = dirs.root + HandleText.parseURL(media['audio'].url).pathname;

            let videos = {
                videoIntro : dirs.root + HandleText.parseURL(media['video-intro'].url).pathname,
                videoOutro : dirs.root + HandleText.parseURL(media['video-outro'].url).pathname
            };
            let videosCreated = images.length;
            let bulletinBoard = false;

            if (_.includes(source.url, 'www.cartasocial')){
                bulletinBoard = true;
            }

            MergeVideos.push(videosCreated, videos, `${tempPath}/${filename}.mp3`, music, tempPath, filename, bulletinBoard, this.callback);
        }


        function videoNormal(){
            return {
                tempPath: tempPath,
                filename: filename,
                width: vidRender.width,
                height: vidRender.height,
                music: true,
                slides: [
                    {
                        narrate: [
                            {text: title},
                            {ssml:'<break time="2s"/>'}
                        ],
                        txtBoxes:[
                            {
                                followContent: true,
                                text: title,
                                maxLines: 7,
                                options: {
                                    paddingX: 40,
                                    paddingY: 40,
                                    lineHeight: 1.4,
                                    textAlign: "center",
                                    verticalAlign: "middle"
                                },
                                fontSize: vidRender.titleSize,
                            }
                        ]
                    },
                    {
                        keep:true,
                        narrate: [
                            {text: desc},
                        ],
                        txtBoxes:[
                            {
                                text: title,
                                maxLines: vidRender.titleLines,
                                truncate: '[...]',
                                options: {
                                    paddingX: 40,
                                    paddingY: 20,
                                    lineHeight: 1.4
                                },
                                fontSize: vidRender.titleSize
                            },
                            {
                                adaptable:true,
                                followContent: true,
                                text: desc,
                                maxLines: vidRender.descLines,
                                options: {
                                    paddingX: 40,
                                    paddingY: 230,
                                    lineHeight: 1.4
                                },
                                fontSize: vidRender.textSize
                            }
                        ]
                    },
                ],
                images: []
            };
        }

        function videoCartaSocial(){
            return {
                tempPath: tempPath,
                filename: filename,
                width: vidRender.width,
                height: vidRender.height,
                music: true,
                slides: [
                    {
                        narrate: [
                            {text: title},
                            {ssml:'<break time="2s"/>'}
                        ],
                        txtBoxes:[
                            {
                                followContent: true,
                                text: title,
                                maxLines: 7,
                                options: {
                                    paddingX: 40,
                                    paddingY: 40,
                                    lineHeight: 1.4,
                                    textAlign: "center",
                                    verticalAlign: "middle"
                                },
                                fontSize: vidRender.titleSize,
                            }
                        ]
                    },
                    {
                        keep:true,
                        loop:vidRender.cartaSocialLoop,
                        txtBoxes:[
                            {
                                text: title,
                                maxLines: vidRender.titleLines,
                                truncate: '[...]',
                                options: {
                                    paddingX: 40,
                                    paddingY: 20,
                                    lineHeight: 1.4
                                },
                                fontSize: vidRender.titleSize
                            },
                            {
                                followContent: true,
                                text: desc,
                                maxLines: vidRender.cartaSocialdescLines,
                                options: {
                                    paddingX: 40,
                                    paddingY: 150,
                                    lineHeight: 1.2,
                                    allowNewLine: true,
                                },
                                fontSize: vidRender.textSize
                            }
                        ]
                    },
                ],
                images: []
            };
        }

    }
}


// .     .       .  .   . .   .   . .    +  .
//   .     .  :     .    .. :. .___---------___.
//        .  .   .    .  :.:. _".^ .^ ^.  '.. :"-_. .
//     .  :       .  .  .:../:            . .^  :.:\.
//         .   . :: +. :.:/: .   .    .        . . .:\
//  .  :    .     . _ :::/:               .  ^ .  . .:\
//   .. . .   . - : :.:./.                        .  .:\
//   .      .     . :..|:                    .  .  ^. .:|
//     .       . : : ..||        .                . . !:|
//   .     . . . ::. ::\(                           . :)/
//  .   .     : . : .:.|. ######              .#######::|
//   :.. .  :-  : .:  ::|.#######           ..########:|
//  .  .  .  ..  .  .. :\ ########          :######## :/
//   .        .+ :: : -.:\ ########       . ########.:/
//     .  .+   . . . . :.:\. #######       #######..:/
//       :: . . . . ::.:..:.\           .   .   ..:/
//    .   .   .  .. :  -::::.\.       | |     . .:/
//       .  :  .  .  .-:.":.::.\             ..:/
//  .      -.   . . . .: .:::.:.\.           .:/
// .   .   .  :      : ....::_:..:\   ___.  :/
//    .   .  .   .:. .. .  .: :.:.:\       :/
//      +   .   .   : . ::. :.:. .:.|\  .:/|
//      .         +   .  .  ...:: ..|  --.:|
// .      . . .   .  .  . ... :..:.."(  ..)"
//  .   .       .      :  .   .: ::/  .  .::\