// create a custom timestamp format for log statements
var winston = require('winston');
require('winston-daily-rotate-file');

export const infoLog = (data) => {

    var transport = new winston.transports.DailyRotateFile({
        filename: `${__dirname}/../logs/info.log`,
        datePattern: 'yyyy-MM-dd.',
        prepend: true,
        level: process.env.ENV === 'development' ? 'debug' : 'info'
    });

    var logger = new (winston.Logger)({
        transports: [
            transport
        ]
    });

    logger.info(data);
};

export const errorLog = (data) => {

    var transport = new winston.transports.DailyRotateFile({
        filename: `${__dirname}/../logs/error.log`,
        datePattern: 'yyyy-MM-dd.',
        prepend: true,
        level: process.env.ENV === 'development' ? 'debug' : 'info'
    });

    var logger = new (winston.Logger)({
        transports: [
            transport
        ]
    });

    logger.error(data);
};

export const successLog = (data) => {

    var transport = new winston.transports.DailyRotateFile({
        filename: `${__dirname}/../logs/success.log`,
        datePattern: 'yyyy-MM-dd.',
        prepend: true,
        level: process.env.ENV === 'development' ? 'debug' : 'info'
    });

    var logger = new (winston.Logger)({
        transports: [
            transport
        ]
    });

    logger.info(data);
};


export const requestsLog = (data) => {

    var transport = new winston.transports.DailyRotateFile({
        filename: `${__dirname}/../logs/requests.log`,
        datePattern: 'yyyy-MM-dd.',
        prepend: true,
        level: process.env.ENV === 'development' ? 'debug' : 'info'
    });

    var logger = new (winston.Logger)({
        transports: [
            transport
        ]
    });

    logger.info(data);
};