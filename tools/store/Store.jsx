/*
Storage built using the singleton module

@TODO localStorage file to check if token still active
*/

class Store {
    constructor() {
        this.token = null;
        this.filters = null;
    }
}

export default (new Store);