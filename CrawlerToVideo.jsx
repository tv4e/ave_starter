'use strict';
import {dirs} from './configs/Config.jsx';
import * as RequestsVideo from './tools/actions/RequestsVideo';

import _ from 'lodash';
import WebScrapper from './tools/WebScrapper';

import DynamicVideo from './tools/ffmpeg/DynamicVideo';

import fs from 'fs-extra';
import tmp from 'tmp';
import uuid from 'node-uuid';
import HandleText from './tools/HandleText';
import CartaSocial from './tools/ffmpeg/webModels/CartaSocial';
import {infoLog, errorLog, successLog} from './tools/Logger';

RequestsVideo.login(() => {
  _checkDirs();
  RequestsVideo.getSource(crawlStep_1);
});

function crawlStep_1(_response) {
  _response.forEach(source => {
    new CrawlerToVideo(source).run();
  });
}

function _checkDirs() {
  _.forEach(dirs, dir => {
    fs.ensureDirSync(dir)
  });
}

class CrawlerToVideo {

  constructor(data) {
    //are private for the instances created
    this.audioTime = null;
    this.media = {};
    this.source = data;
  }

  run() {
    infoLog(`Starting New Process`);
    this.crawlStep_2();
  }

  crawlStep_2() {
    let source = this.source;
    let self = this;

    if (!_.isEmpty(source.rel_subs)) {
      _.forEach(source.rel_subs, (rel_sub) => {
        let subSource = _.cloneDeep(source);
        subSource.url = source.url + rel_sub.url;
        subSource.html_exception = source.html_exception;
        subSource.subId = rel_sub.subId;

        //content means is single static page nothing more
        if (rel_sub.content) {
          new WebScrapper((err, webResults) => {
            if (err)
              return null;
            source.subId = subSource.subId;
            filter(webResults, rel_sub, 1);
          }).singleCrawl(subSource);
        } else {
          if (_.includes(source.url, 'www.cartasocial')) {
            new WebScrapper((err, webResults) => {
              if (err) {
                return null;
              }

              let content = CartaSocial.handle(webResults, rel_sub.name, rel_sub.resposta);
              RequestsVideo.checkVideo(content.title, subSource.subId, 1, (response) => {
                source.subId = subSource.subId;

                if (response.status == 'NOK') {
                  self.crawlStep_3(content, source);
                }
              });

            }).crawl(subSource, {encoding: null}, true);
          } else {
            new WebScrapper((err, webResults) => {
              if (err)
                return null;
              source.subId = subSource.subId;
              filter(webResults, rel_sub, 0);
            }).crawl(subSource);
          }
        }

      });

    } else {
      new WebScrapper((err, webResults) => {
        if (err)
          return null;
        filter(webResults, source);
      }).crawl(source);
    }


    function filter(webResults, _source, _sub) {

      _.forEach(webResults, webResult => {
        HandleText.filtersPromise(webResult.description, (promise) => {
          if (promise) {
            let sub = 0;
            let id = _source.id;
            if (_source.subId) {
              id = _source.subId;
              sub = 1;
            }
              RequestsVideo.checkVideo(content.title, subSource.subId, 1, (response) => {
                source.subId = subSource.subId;

                if (response.status == 'NOK') {
                  self.crawlStep_3(content, source);
                }
              });
          }
        });
      });

    }

  }

  crawlStep_3(webResult, source) {
    let self = this;
    let subId = null;
    if (_.includes(source.url, 'www.cartasocial')) {
      subId = source.subId;
    }

    tmp.dir({mode: 0o777, template: '/tmp/tmp-XXXXXX',}, function _tempDirCreated(err, path) {
      if (err) throw err;
      let filename = uuid.v4();
      let temp = path;

      RequestsVideo.getAsgie(self.source.asgie_id, (media) => {
        if (_.includes(source.url, 'www.cartasocial')) {
          source.subId = subId;
        }
        self.crawlStep_4(webResult, media.resources, temp, filename, source);
      });
    });

  }

  crawlStep_4(webResult, media, temp, filename, source) {
    let _source = source;
    let isSub = false;

    if (!_.isEmpty(this.source.rel_subs)) {
      _source = _.cloneDeep(source);
      _source.url = this.source.url + source.url;
      _source.rel_asgie = {color: this.source.rel_asgie.color};
      isSub = true;
      _source.id = source.subId;
    }

    new DynamicVideo({
      source: _source,
      template: "template1",
      foregroundColor: this.source.rel_asgie.color,
      media: media,
      tempPath: temp,
      title: webResult.title,
      desc: webResult.description,
      filename: filename,
    }, function (duration) {
     // remove temp directory
       RequestsVideo.putVideo({
        source_id: _source.id,
        title: webResult.title,
        filename: filename + ".mp4",
        desc: webResult.description,
        isSub: isSub,
        duration: Math.round(duration),
      }, () => {
        // remove temp directory
        fs.remove(`${temp}`, err => {
          if (err) return console.error(err)

        });
        
        console.log("\n\nVideo processed was successful");
        console.log("=> Updating database");
        successLog(`File Name: ${filename} | source: ${_source.url}`);
      });
    });
  }
}
