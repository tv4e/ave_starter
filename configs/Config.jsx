'use strict';

// should be something like     api = http://example.com"
export let api = NULL;

export let dirs = {
        root:'/assets/+tv4e',
        images:'/assets/+tv4e/images',
        video: '/assets/+tv4e/videos/informative',
        audio:'/assets/+tv4e/audios'
};

export let videosConcurrency = 3;

export let vidRender = {
        titleSize:'60px Tiresias',
        titleBigSize:'70px Tiresias',
        textSize:'50px Tiresias',
        titleLines: 2,
        descLines: 5,
        cartaSocialdescLines: 5,
        cartaSocialLoop: 10,
        width:1280,
        height:720,
        ySpacing:75,
        // @TODO set here distance to title firstYdesc:'SET HERE'
};